/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sources.beans;

/**
 *
 * @author matheus
 */
public class funcionario {
    private int codigo;
    private String nome;
    private int codigo_cargo;

    public funcionario(int codigo, String nome, int codigo_cargo) {
        this.codigo = codigo;
        this.nome = nome;
        this.codigo_cargo = codigo_cargo;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the codigo_cargo
     */
    public int getCodigo_cargo() {
        return codigo_cargo;
    }

    /**
     * @param codigo_cargo the codigo_cargo to set
     */
    public void setCodigo_cargo(int codigo_cargo) {
        this.codigo_cargo = codigo_cargo;
    }
    
}
