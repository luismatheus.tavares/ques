#include <iostream>
#include <vector>

using namespace std;

class cotacao{
    public:

    void adicionar(string titulo){
        titulos.emplace_back(titulo);
    }

    //o metodo eh boleano por redundancia
    bool remover(string titulo){
        for(int i = 0 ; i < titulos.size() ; i++){
            if(titulos[i] == titulo){
                titulos.erase(titulos.cbegin()+i);
                return true;
            }
        }
        return false;
    }

    //o metodo eh boleano por redundancia
    bool remover(int pos){
        if(pos < 0 || pos > titulos.size()-1){
            return false;
        }
        titulos.erase(titulos.cbegin()+pos);
        return true;
    }

    //o metodo eh boleano por redundancia
    bool alterar(int pos, string novo_titulo ){
        if(pos < 0 || pos > titulos.size()-1){
            return false;
        }
        titulos[pos] = novo_titulo;
        return true;
    }

    void print_cotacoes(){
        for(string s: titulos){
            cout << s << endl;
        }
    }

    private:
    vector<string> titulos;
};