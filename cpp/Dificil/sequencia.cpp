#include <iostream>
#include <vector>

using namespace std;

class sqc{
    public:
    int getIndex(int stream[],int n){
        
        //valor que armazena a maior sequencia de 1;
        int maior_sequencia = 0;
        //o indice onde se encotra o valor otimo
        int index = 0;

        //variaveis auxiliares
        int sequencia_temp = 0;
        vector<int> stream_tmp;

        //copiando o array de entrada em um vetor
        for(int i = 0 ; i < n ; i++){
            stream_tmp.emplace_back(stream[i]);
        }
        
        //percorrendo o vetor
        for (int i = 0; i < n; i++){
            //caso o valor seja zero
            if(stream[i] == 0){
                //temporariamente transforma em 1 para verificar se o valor otimo eh encontrado
                stream_tmp[i] = 1;

                //percorre os numeros a direita de i (maiores)
                for(int j = i ; j < n ; j++){
                    //caso seja 1 o contador temporario incrementa
                    if(stream_tmp[j] == 1){
                        sequencia_temp++;
                    //caso nao seja 1 o loop eh encerrado
                    }else{
                        break;
                    }
                }
                //percorre os numeros a esquerda de i (menores)
                for(int j = i ; j > 0 ; j--){
                    //caso seja 1 o contador temporario incrementa
                    if(stream_tmp[j] == 1){
                        sequencia_temp++;
                    //caso nao seja 1 o loop eh encerrado
                    }else{
                        break;
                    }
                }

                //caso a sequencia atual seja maior que a anterior armazene a quantidade da sequencia e o indice
                if(sequencia_temp > maior_sequencia){
                    maior_sequencia = sequencia_temp ;
                    index = i;
                }
                sequencia_temp = 0;
                
                //o valor eh transformado em zero apos o fim da verficacao
                stream_tmp[i] = 0;
            }
        }
        // o indice e adicionado em 1
        index++;

        //print
        cout << index << endl;

        return index;
    };
};