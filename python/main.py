from cotacoes import cotacoes
from maior_num import compare
from maior_prod import maiorProd
from xadrez import checkBoard
from seq_zeros import checkSequence
from robson_construc import *

def main():
        print('COTACOES')
        c = cotacoes()
        print('adicionando dois titulos')
        c.addCotacoes('titulo 1')
        c.addCotacoes('titulo 2')

        c.printTitulos()

        print('removendo "titulo 1"')
        c.delCotacoes("titulo 1")
        c.printTitulos()

        print('alterando o titulo restante')
        c.chngCotacoes(0,"unico titulo")
        c.printTitulos()
        
        print('-----------------------------------')

        print('COMPARACOES')

        print("compararndo 10 e 5:")
        compare(10,5)
        print("compararndo 8 e 12:")
        compare(8,12)

        print('-----------------------------------')

        print("MAIOR PRODUTO")

        print("maior produto do array [-6,4,-5,8,-10,0,8]:")

        maiorProd([-6,4,-5,8,-10,0,8])

        print('-----------------------------------')

        print("PECAS DE XADREZ")

        board =[[4,3,2,5,6,2,3,4],
                [1,1,1,1,1,1,1,1],
                [0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0],
                [1,1,1,1,1,1,1,1],
                [4,3,2,5,6,2,3,4]]

        table = {"peao":1,
                "bispo":2,
                "cavalo":3,
                "torre":4,
                "rainha":5,
                "rei":6}

        checkBoard(table,board)
        print('-----------------------------------')

        print("SEQUENCIAS DE 1")

        print("melhor index pra sequencia de 1:")
        checkSequence([1,0,1,1,0,0,0,1,1,1,1,0])

        print('-----------------------------------')

        print("ROBSON CONSTUCOES")

        cons = construc()

        cons.addCargo(350)
        cons.addCargo(720)

        cons.addFunc(funcionario(58,"matheus",0))
        cons.addFunc(funcionario(12,"pedro m",0))
        cons.addFunc(funcionario(99,"adriana",1))
        cons.addFunc(funcionario(2,"debora",0))
        cons.addFunc(funcionario(74,"karine",1))
        #funcionario invalido
        cons.addFunc(funcionario(61,"larissa",2))

        cons.relatorioFunc()

        cons.relatorioCargo(0)

        cons.relatorioCargo(1)

if __name__ == "__main__":
        main()