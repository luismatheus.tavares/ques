def maiorProd(array):
    #armazena os numeros positivos
    varPos = []
    #armazena os numeros negativos
    varNeg = []
	
    #percorre o array separando os numeros em negativos e positivos(0 eh sempre ignorado)
    for i in array:
        if i > 0:
            varPos.append(i)
        elif i < 0:
            varNeg.append(i)
        else:
            continue
    #se a quantidade de numeros negativos em um array for impar: retirar o mais proximo de 0
    #se a quantidade for par a multiplicacao dos sinais sempre vai resultar em um numero positivo(pular o if)
    if len(varNeg) % 2 != 0:
        maior_valor = -float('inf')
        for i in varNeg:
            if i > maior_valor:
                maior_valor = i
        varNeg.remove(maior_valor)
    
    #inserindo os numeros negativos na resposta(array de numeros positivos)
    varPos.extend(varNeg)

    #print
    print('{',end="")
    for i in varPos:
        print(f' {i} ',end="")
    print('}')

    return varPos
