def checkSequence(array):
    #valor que armazena a maior sequencia de 1
    maior_sequencia = 0
    # o indice onde se encotra o valor otimo
    index = 0

    #valiaveis auxiliares
    sequencia_temp = 0
    array_temp =[]

    #copiando o array de entrada
    array_temp = array.copy()

    #percorrendo o array copia
    for i in range(len(array_temp)):
        #caso o valor seja zero
        if array_temp[i] == 0:
            #temporariamente transforma em 1 para verificar se o valor otimo eh encontrado
            array_temp[i] = 1
            #percorre o array copia verificando os valores

            #percorre os numeros a direita de i (maiores)
            j = i
            while j < len(array_temp):
                #caso seja 1 o contador temporario incrementa
                if array_temp[j] == 1:
                    sequencia_temp += 1
                    j+=1
                #caso nao seja 1 o loop eh encerrado
                else:
                    break

            j = i
            #percorre os numeros a esquerda de i (menores)
            while j > 0:
                #caso seja 1 o contador temporario incrementa
                if array_temp[j] == 1:
                    sequencia_temp += 1
                    j-=1
                #caso nao seja 1 o loop eh encerrado
                else:
                    break
            
            #caso a sequencia atual seja maior que a anterior armazene a quantidade da sequencia e o indice
            if sequencia_temp > maior_sequencia:
                maior_sequencia = sequencia_temp
                index = i

            sequencia_temp = 0

            #o valor eh transformado em zero apos o fim da verficacao
            array_temp[i] = 0
    
    #o indice e adicionado em 1
    index +=1

    #print
    print(index)

    return index
    